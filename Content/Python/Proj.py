import sys
import urllib2
class Item:
    title = ''
    id = ''
    price = ''
    image = ''
    seller = ''
    sellerUrl = ''
    url=''
class Text:
 def add(self, path):
    n = Item()
    page = urllib2.urlopen(path)
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(page, 'html.parser')
    try:
        n.price = soup.find("span", {"id": "mm-saleDscPrc"}).text

    except:
        n.price = soup.find(attrs={'itemprop': 'price'})['content'] + ' ' + \
                  soup.find(attrs={'itemprop': 'priceCurrency'})['content']

    n.title = soup.title.string
    n.image = soup.find(attrs={'itemprop': 'image'})['src']

    n.sellerUrl = soup.find("a", {"id": "mbgLink"})['href']
    n.seller = soup.find("a", {"id": "mbgLink"}).text
    return n
 def getList(self,path):
    page = urllib2.urlopen(path)
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(page, 'html.parser')
    ul = soup.find("ul", {"id": "ListViewInner"})
    list = []
    for li in ul.findAll('li', {"class": "sresult lvresult clearfix li"}):
        t = Item()
        t.title = li.find("a", {"class": "vip"}).text
        try:
         t.image = li.find("img")["imgurl"]
        except:
         t.image = li.find("img")["src"]
        t.price = li.find("span", {"class": "bold"}).text
        t.url = li.find("a", {"class": "vip"})["href"]
        list.append(t)
    return list